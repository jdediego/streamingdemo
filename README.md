# Streaming API Demo


## Purpose

We want to show a bidirectional stream in a gRPC endpoint that can be used to produce and consumer messages to/from Kafka topics.

As Kafka consumers gRPC clients can manage how the proxy consumes Kafka topics by handling variables like the offset number.

Kafka consumer load balancing can be replaced by K8s HA features of the proxy service. A smart gRPC client will be able to handling variables like the offset number and apply policies.

## Requirements

- Docker Compose
- gRPC client (e.g. BloomRPC) to inter-act with the server
- Kafka Manager (e.g Conduktor) to monitor the Kafka topics

## Run the Demo
NOTE: First of all, please install go in your local machine and change the app path in the go and Dockerfile files.

- Clone this repository.
- cd to microservice.
- Compile the protobuf files.
- Run the docker compose file from the root dir (Docker will build an image with the app in the folder 'microservice').

```
docker compose up
```
- Connect to the Kafka cluster using a Kafka client like [Conduktor](https://www.conduktor.io/) at localhost:9092
- Connect the RPC client like [BloomRPC](https://github.com/uw-labs/bloomrpc) and load the protobuf file (microservice/protobuf/streaming.proto). Pick up the SubscribeEvents service.
- Send the first message to connect. The microservice will send all messages from the client to the topic demo-in.

### Simulation of Messages from Kafka
The microservice will generate a message every 5 seconds to the topic demo-out. You should be able to receive the messages this topic in your gRPC client as the incoming stream.