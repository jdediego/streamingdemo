module jdediego/streamingdemo/microservice

go 1.15

require (
	github.com/google/uuid v1.1.2
	github.com/segmentio/kafka-go v0.4.10
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.7.1
	golang.org/x/net v0.0.0-20210226172049-e18ecbb05110
	golang.org/x/tools v0.0.0-20191112195655-aa38f8e97acc
	google.golang.org/grpc v1.36.0
	google.golang.org/protobuf v1.25.0
)
