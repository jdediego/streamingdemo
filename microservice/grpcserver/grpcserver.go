package grpc

import (
	"fmt"
	"log"
	"encoding/json"
	"io"
	configuration "jdediego/streamingdemo/microservice/configuration"
	kafka "jdediego/streamingdemo/microservice/kafka"
	pb "jdediego/streamingdemo/microservice/protobuf"
	utils "jdediego/streamingdemo/microservice/utils"
)

const componentMessage = "GRPC Server"

var config = configuration.GlobalConfiguration

type NotificationsService struct {
	event *pb.Event
}

func NewNotificationsService(event *pb.Event) *NotificationsService {
	return &NotificationsService{event: event}
}

func (c *NotificationsService) SubscribeNotifications(stream pb.NotificationsService_SubscribeNotificationsServer) error{
	log.Println("Started stream")
	waitout := make(chan struct{})
	go func() {
		listen(stream)
	}()
	go func() {
		kafka.StartListening(stream)
	}()
	<-waitout
	return nil
}


func listen(stream pb.NotificationsService_SubscribeNotificationsServer) {
	methodMsg := "SubscribeNotifications"
	for {
		in, err := stream.Recv()
		if err == io.EOF {
			continue
		}
		if err != nil {
			continue
		}
		messageByte, jsonErr := json.MarshalIndent(in, "", "\t")
		if jsonErr != nil {
			utils.PrintLogError(jsonErr, componentMessage, methodMsg, fmt.Sprintf("Error marshaling input message - Reason '%s'", jsonErr.Error()))
		}
		sendToKafkaErr := kafka.SendEventMessageToTopic(string(messageByte), configuration.GlobalConfiguration.Kafka.Intopic)
		if sendToKafkaErr != nil {
			utils.PrintLogError(sendToKafkaErr, componentMessage, methodMsg, fmt.Sprintf("Error sending input message to topic - Reason '%s'", sendToKafkaErr.Error()))
		}
		log.Println("Got message to topic OK: " + string(messageByte))
	}
}




