package kafka

import (
	"context"
	"encoding/json"
	"fmt"
	configuration "jdediego/streamingdemo/microservice/configuration"
	utils "jdediego/streamingdemo/microservice/utils"
	"strings"
	"time"
	pb "jdediego/streamingdemo/microservice/protobuf"
	kafka "github.com/segmentio/kafka-go"
)

const componentMessage = "Topics Consumer Service"

var config = configuration.GlobalConfiguration
var startOffset int64


func getKafkaReader() *kafka.Reader {
	broker := config.Kafka.Bootstrapserver
	brokers := strings.Split(broker, ",")
	topic := config.Kafka.Outtopic

	return kafka.NewReader(kafka.ReaderConfig{
		Brokers:  brokers,
		Partition: 0,
		Topic:    topic,
		MinBytes: config.Kafka.Messageminsize,
		MaxBytes: config.Kafka.Messagemaxsize,
		MaxWait:  100 * time.Millisecond,
		CommitInterval: time.Second,
	})
}

func StartListening(stream pb.NotificationsService_SubscribeNotificationsServer) {
	methodMsg := "StartListening"
	reader := getKafkaReader()
	//reader.SetOffset(20)
	defer reader.Close()
	for {
		//m, err := reader.ReadMessage(context.Background())
		m, err := reader.FetchMessage(context.Background()) // explicit commit
		if err != nil {
			utils.PrintLogError(err, componentMessage, methodMsg, fmt.Sprintf("Error reading message - Reason: %s", err.Error()))
		}
		msg := fmt.Sprintf("Message at topic:%v partition:%v offset:%v	%s = %s\n", m.Topic, m.Partition, m.Offset, string(m.Key), string(m.Value))
		utils.PrintLogInfo(componentMessage, methodMsg, msg)
		event, eventErr := convertMessageToProcessable(m)
		if eventErr == nil {
			utils.PrintLogInfo(componentMessage, methodMsg, fmt.Sprintf("Message converted to event successfully - Key '%s'", m.Key))
			event.Offset = m.Offset
			sendErr := stream.Send(event)
			if sendErr != nil {
				utils.PrintLogError(eventErr, componentMessage, methodMsg, fmt.Sprintf("Send output message to stream failed - Reason '%s'", sendErr.Error()))
			}
		}
	}
}

func convertMessageToProcessable(msg kafka.Message) (*pb.Event, error) {
	methodMsg := "convertMessageToProcessable"
	var event pb.Event
	unmarshalErr := json.Unmarshal(msg.Value, &event)
	if unmarshalErr != nil {
		//utils.PrintLogWarn(unmarshalErr, componentMessage, methodMsg, fmt.Sprintf("Error unmarshaling message content to JSON - Key '%s'", msg.Key))
		return &event, unmarshalErr
	}
	utils.PrintLogInfo(componentMessage, methodMsg, fmt.Sprintf("ID '%s'", event.Id))
	return &event, nil
}
