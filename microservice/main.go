package main

import (
	"fmt"
	"net"
	"strconv"
	"time"
	"encoding/json"
	"strings"
	"math/rand"
	"google.golang.org/grpc"
	grpcserver "jdediego/streamingdemo/microservice/grpcserver"
	pb "jdediego/streamingdemo/microservice/protobuf"
	utils "jdediego/streamingdemo/microservice/utils"
	configuration "jdediego/streamingdemo/microservice/configuration"
	kafka "jdediego/streamingdemo/microservice/kafka"
	"github.com/google/uuid"
)

const componentMsg = "gRPC Server"
var config = configuration.GlobalConfiguration


func startScheduledOutMessages(){
	methodMessage := "startScheduledOutMessages"
	for true {
		time.Sleep(time.Duration(5) * time.Second)
		utils.PrintLogInfo(componentMsg, methodMessage, "Scheduled action to send messages to topic demo-out starts: %s")
		message := getOutMesssage()
		sendErr := kafka.SendEventMessageToTopic(message, config.Kafka.Outtopic)
		if sendErr != nil {
			utils.PrintLogError(sendErr, componentMsg, methodMessage, "Error sending msg to demo-out")
		}
	}
}

func getOutMesssage() string {
	tm := time.Now() 
	mails := []string{"bill@yahoo.com", "john@fake.com", "joe@gmail.com", "Gavin@msn.com"}
	greetings := []string{"bill@yahoo.com", "john@fake.com", "joe@gmail.com", "Gavin@msn.com"}
    randomIndexMail := rand.Intn(len(mails))
	randomIndexMsg := rand.Intn(len(greetings))
    pickEMail := mails[randomIndexMail]
	pickGreeting := greetings[randomIndexMsg]
    
	var msg pb.Event
	msg.AuthorEmail = pickEMail
	msg.AuthorWhen = fmt.Sprintf("%s", tm) 
	msg.Message = pickGreeting
	msg.Id = getUUID()

	messageByte, jsonErr := json.MarshalIndent(msg, "", "\t")
	if jsonErr != nil {
		utils.PrintLogError(jsonErr, componentMsg, "getOutMesssage", "Error marshaling msg to demo-out")
	}
	return string(messageByte)
}

func getUUID() string{
    uuidWithHyphen := uuid.New()
    fmt.Println(uuidWithHyphen)
    uuid := strings.Replace(uuidWithHyphen.String(), "-", "", -1)
    fmt.Println(uuid)
	return uuid
}

func main() {
	go startScheduledOutMessages()

	server := grpc.NewServer()
	service := pb.NotificationsServiceServer(&grpcserver.NotificationsService{})
	pb.RegisterNotificationsServiceServer(server, service)

	// Start gRPC service's server	
	grpcPort := config.Grpcserver.Port
	listener, listenerErr := net.Listen("tcp", fmt.Sprintf(":%d", grpcPort))
	if listenerErr != nil {
		utils.PrintLogError(listenerErr, componentMsg, "Starting", "Error")
	}
	utils.PrintLogInfo(componentMsg, "Starting", "Starting microservice gRPC server on port "+strconv.Itoa(grpcPort))

	if err := server.Serve(listener); err != nil {
		utils.PrintLogError(listenerErr, componentMsg, "Grpc Server start", "Error")
	}

}
