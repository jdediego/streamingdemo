package utils

const Event_received_pre_ok = "EVENT RECORD RECEIVED OK"
const Event_batch_received_pre_ok = "EVENT BATCH RECEIVED OK"

const Event_post_ok="EVENT RECORD PROCESSED OK"
const Event_post_batch_ok="EVENT BATCH PROCESSED OK"
const Event_post_error_server="EVENT RECORD PROCESSED SERVER ERROR"
const Event_post_error_client="EVENT RECORD PROCESSED CLIENT ERROR"

const Event_batch_received_post_ok = "EVENT BATCH PROCESSED OK"

const Event_delete_ok="EVENT DELETE RECORD PROCESSED OK"
const Event_delete_error_server="EVENT DELETE RECORD PROCESSED SERVER ERROR"
const Event_delete_error_client="EVENT DELETE RECORD PROCESSED CLIENT ERROR"

const Query_byID_ok="GET BYID RECEIVED OK"
const Query_byquery_ok="GET BYQUERY RECEIVED OK"

const Event_topic_ok="EVENT RECORD OK"
const Event_topic_error="EVENT RECORD ERROR"

const Event_topic_delete_ok="EVENT DELETE RECORD OK"
const Event_topic_delete_error="EVENT DELETE RECORD FAIL"



